
<%@page import="entity.MatrixHistory"%>

<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<title>History</title>
<% List<MatrixHistory> histories = (List<MatrixHistory>) request.getAttribute("histories"); %>

</head>
<body>


 	<nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header" style="font-family: Calibri">
          <a class="navbar-brand" href="/MatricesMultiplier/index.jsp">MatricesMultiplier</a>
        </div>
        <div>
          <ul class="nav navbar-nav" style="font-family: Calibri">
            <li><a href="/MatricesMultiplier/ControllerServlet?par=mat1">Matrix1</a></li>
            <li><a href="/MatricesMultiplier/ControllerServlet?par=mat2">Matrix2</a></li>
            <li class="active"><a href="/MatricesMultiplier/ControllerServlet?par=his">History</a></li>
          </ul>
        </div>
      </div>
    </nav>
    
            
<div class="container">                        
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>ID</th>
            <th>Date</th>
            <th>Options</th>
          </tr>
        </thead>
        <tbody>
        	<% for(MatrixHistory history : histories) { %>
          	<tr>
          		<td><%=history.getId()%></td>
            	<td><%=history.getDate()%></td>
            	<td> 
            		<a href="/MatricesMultiplier/ControllerServlet?par=shis&id=<%=history.getId()%>" class="btn btn-default btn-sm">
         				<span class="glyphicon glyphicon-eye-open"></span> View 
         			</a>
       				<a href="/MatricesMultiplier/ControllerServlet?par=dhis&id=<%=history.getId()%>" class="btn btn-default btn-sm">
          				<span class="glyphicon glyphicon-remove"></span> Remove 
       				</a>
       			</td>
         	</tr>
          <% } %>
        </tbody>
      </table>
    </div>
</body>
</html>