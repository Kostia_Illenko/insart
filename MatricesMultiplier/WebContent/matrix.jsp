<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="entity.Matrix" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>MatricesMultiplier</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<% Matrix matrix = (Matrix) request.getAttribute("matrix");
		String atr = (String) request.getAttribute("par");
	%>	
</head>
<body>

	<nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header" style="font-family: Calibri">
          <a class="navbar-brand" href="/MatricesMultiplier/index.jsp">MatricesMultiplier</a>
        </div>
        <div>
          <ul class="nav navbar-nav" style="font-family: Calibri">
           <% if(atr.equals("mat1")) {%> 
            <li class="active"><a href="/MatricesMultiplier/ControllerServlet?par=mat1">Matrix1</a></li>
            <% } else { %>
            <li><a href="/MatricesMultiplier/ControllerServlet?par=mat1">Matrix1</a></li>
            <% } %>
            <% if(atr.equals("mat2")) {%> 
            <li class="active"><a href="/MatricesMultiplier/ControllerServlet?par=mat2">Matrix2</a></li>
            <% } else { %>
            <li><a href="/MatricesMultiplier/ControllerServlet?par=mat2">Matrix2</a></li>
            <% } %>
            <li><a href="/MatricesMultiplier/ControllerServlet?par=his">History</a></li>
          </ul>
        </div>
      </div>
    </nav>
    
    <div class="container">
    <form role="form" action="/MatricesMultiplier/ControllerServlet" method="post">
	<table class="table table-bordered">
		<tbody> <%! int idOf = 0; %>
			<% for(int i = 0; i < matrix.getNumberOfRows(); i++) { %>
			<tr> 
				<% for(int j = 0; j < matrix.getNumberOfCols(); j++) { %>
					<td style="text-align: center"> 
 						 <div class="form-group"><input type="number" class="form-control" name="<%=new String("c" + idOf)%>" value="<%= matrix.getElement(i, j)%>"> 
					</td>
				<% idOf++; 
					} %>
			</tr>
			<% } idOf = 0;  %>
		</tbody>
	</table>
	<input type="hidden" value="<%= matrix.getNumberOfRows()%>" name="numberOfRows">
					<input type="hidden" value="<%= matrix.getNumberOfCols()%>" name="numberOfCols">
					<input type="hidden" value="ed" name="par">
					<input type="hidden" value="<%=atr%>" name="mat">
  	<button type="submit" class="btn btn-default">Save</button>
	</form>
	</div>    
</body>
</html>