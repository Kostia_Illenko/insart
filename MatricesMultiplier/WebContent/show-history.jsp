<%@page import="entity.Matrix"%>
<%@page import="entity.MatrixHistory"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<title>Show History</title>
<%  MatrixHistory history = (MatrixHistory) request.getAttribute("matrixhistory");
	Date date = history.getDate();
	Matrix matrix1 = history.getMatrixFirst();
	Matrix matrix2 = history.getMatrixSecond();
	Matrix result = history.getMatrixResult();
%>	
</head>
<body>

	<nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header" style="font-family: Calibri">
          <a class="navbar-brand" href="/MatricesMultiplier/index.jsp">MatricesMultiplier</a>
        </div>
        <div>
          <ul class="nav navbar-nav" style="font-family: Calibri">
            <li><a href="/MatricesMultiplier/ControllerServlet?par=mat1">Matrix1</a></li>
            <li><a href="/MatricesMultiplier/ControllerServlet?par=mat2">Matrix2</a></li>
            <li class="active"><a href="/MatricesMultiplier/ControllerServlet?par=his">History</a></li>
          </ul>
        </div>
      </div>
    </nav>


	<div class="container">
		<h2><%=date%></h2>
	</div>
	
	<div class="container">
      <h2>First Matrix</h2>                    
      <table class="table table-bordered">
        <tbody>
        	<%for(int i = 0; i < matrix1.getNumberOfRows(); i++) { %>
          		<tr>
          			<%for(int j = 0; j < matrix1.getNumberOfRows(); j++) { %>
            			<td><%=matrix1.getElement(i, j)%></td>
            		<% } %>
          		</tr>
          	<% }  %>
         
        </tbody>
      </table>
    </div>
    
    <div class="container">
      <h2>Second Matrix</h2>                    
      <table class="table table-bordered">
        <tbody>
        	<%for(int i = 0; i < matrix2.getNumberOfRows(); i++) { %>
          		<tr>
          			<%for(int j = 0; j < matrix2.getNumberOfRows(); j++) { %>
            			<td><%=matrix2.getElement(i, j)%></td>
            		<% } %>
          		</tr>
          	<% }  %>
         
        </tbody>
      </table>
    </div>
    
    <div class="container">
      <h2>Result Matrix</h2>                    
      <table class="table table-bordered">
        <tbody>
        	<%for(int i = 0; i < result.getNumberOfRows(); i++) { %>
          		<tr>
          			<%for(int j = 0; j < result.getNumberOfRows(); j++) { %>
            			<td><%=result.getElement(i, j)%></td>
            		<% } %>
          		</tr>
          	<% }  %>
         
        </tbody>
      </table>
    </div>
</body>
</html>