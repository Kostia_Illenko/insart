package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entity.Matrix;
import entity.MatrixHistory;
import enums.DAOType;

/**
 * Servlet implementation class ControllerServlet
 */
@WebServlet("/ControllerServlet")
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	Controller controller = new Controller(DAOType.DB);
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String param = request.getParameter("par");
		Controller controller = new Controller(DAOType.DB);
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		request.setAttribute("par", param);
		System.out.println(param);
		switch (param) {
			case "mat1":
				Matrix matrix = controller.readMatrix("matrix1");
				request.setAttribute("matrix", matrix);
				request.getRequestDispatcher("/matrix.jsp").forward(request, response);
				break;
			case "mat2":
				request.setAttribute("matrix", controller.readMatrix("matrix2"));
				request.getRequestDispatcher("/matrix.jsp").forward(request, response);
				break;
			case "ed": 
				String mat = (String) request.getParameter("mat");
				System.out.println(mat);
				String matrixName;
				if(mat.equals("mat1")) {
					matrixName = "matrix1";
				} else { 
					matrixName = "matrix2";
				}
				int numberOfRows = Integer.parseInt((String)request.getParameter("numberOfRows"));
				int numberOfCols = Integer.parseInt((String)request.getParameter("numberOfCols"));
				int counter = 0;
				for(int i = 0; i < numberOfRows; i++) {
					for(int j = 0; j < numberOfCols; j++) {
						controller.updateMatrixByXandY(matrixName, i, j, Integer.parseInt((String)request.getParameter("c" + counter)));
						counter++;
					}
				}
				request.setAttribute("matrix", controller.readMatrix(matrixName));
				request.getRequestDispatcher("/matrix.jsp").forward(request, response);

				break;
			case "his":	
				List<MatrixHistory> histories = controller.getHistory();
				request.setAttribute("histories", histories);
				request.getRequestDispatcher("/history.jsp").forward(request, response);
				break;
			case "shis": 
				int id = Integer.parseInt((String)request.getParameter("id"));
				MatrixHistory history = controller.getMatrixHistory(id);
				request.setAttribute("matrixhistory", history);
				System.out.println(history.getDate());
				request.getRequestDispatcher("/show-history.jsp").forward(request, response);
				break;
			case "dhis": 
				int idd = Integer.parseInt((String)request.getParameter("id"));
				controller.deleteMatrixHistory(idd);
				histories = controller.getHistory();
				request.setAttribute("histories", histories);
				request.getRequestDispatcher("/history.jsp").forward(request, response);
				break;
			default:
				break;
		}
	}

}
