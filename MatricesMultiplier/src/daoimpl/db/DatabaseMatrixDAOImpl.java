package daoimpl.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import dao.MatrixDAO;
import dao.db.AbstractDatabaseDAO;
import entity.Matrix;

public class DatabaseMatrixDAOImpl extends AbstractDatabaseDAO implements MatrixDAO {

	@Override
	public void write(Matrix matrix) {
		openConnection();
		String sql = "INSERT INTO matrix (matrix_name, row_id, col_id, value) " +
				"VALUES (?, ?, ?, ?)";
		
			PreparedStatement stmt = null;
			try {
				stmt = conn.prepareStatement(sql);
			
				ResultSet rs = null;
				for(int i = 0; i < matrix.getNumberOfRows(); i++) {
					for(int j = 0; j < matrix.getNumberOfCols(); j++) {
						stmt.setString(1, matrix.getName());
						stmt.setInt(2, i);
						stmt.setInt(3, j);
						stmt.setInt(4, matrix.getElement(i, j));
						stmt.executeUpdate();
					} 
				}
				
				stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}

	@Override
	public Matrix read(int id) {
		 throw new UnsupportedOperationException("That is the unsopported operation for work with matrix table in the DB");
	}

	@Override
	public void update(Matrix matrix) {	
		 throw new UnsupportedOperationException("That is the unsopported operation for work with matrix table in the DB");
	}

	@Override
	public void delete(int id) {
		openConnection();
		
		String sql = "DELETE FROM matrix WHERE id = ?";
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			
			stmt.setInt(1, id);
			stmt.executeUpdate();

			stmt.close();
		} catch (SQLException e) {
			System.out.println("Delete error");
		}
	}

    @Override
    public void updateMatrixByXandY(String name, int x, int y, int value) {
    	openConnection();
		
		String sql = "UPDATE `matrix` SET `value` = ? WHERE `matrix_name` = ? AND `row_id` = ? AND `col_id` = ?";
			
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setInt(1, value);
			stmt.setString(2, name);
			stmt.setInt(3, x);
			stmt.setInt(4, y);
			stmt.executeUpdate();

			stmt.close();
		} catch (SQLException e) {
			System.out.println("Update error");
		}

		System.out.println("�������������� ������ �������");
    }

    @Override
    public Matrix getMatrixByName(String name) {
    	String sql = "SELECT row_id, col_id, value FROM matrix WHERE matrix_name = ?";
		ResultSet rs = null;
		PreparedStatement stmt = null;

		try {
			

			openConnection();
			System.out.println("isClosed = " + conn.isClosed());
			stmt = conn.prepareStatement(sql);
		
			stmt.setString(1, name);
			rs = stmt.executeQuery();
			
			System.out.println("Database " + name);
			
		}
		catch(SQLException e) {
			System.out.println("Error read");

		}
		int counter = 0;
		try {
			while(rs.next()) {
				counter++;
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		try {
			rs = stmt.executeQuery();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		Matrix matrix = new Matrix((int)Math.sqrt((double)counter), (int)(Math.sqrt((double)counter)));
		try {
			while(rs.next()) {
				matrix.setElement(rs.getInt("row_id"), rs.getInt("col_id"), rs.getInt("value"));
			}

			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return matrix;
    }

	@Override
	public void deleteMatrixByName(String name) {
		openConnection();
		
		String sql = "DELETE FROM matrix WHERE matrix_name = ?";
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			
			stmt.setString(1, name);
			stmt.executeUpdate();

			stmt.close();
		} catch (SQLException e) {
			System.out.println("Delete error");
		}
	}
}
