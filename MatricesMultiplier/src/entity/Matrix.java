package entity;

import java.io.Serializable;

public class Matrix implements Serializable {
	private int numberOfRows;
	private int numberOfCols;
	private int[][] data;
    private String name;
	
	public Matrix(int numberOfRows, int numberOfCols) {
		super();
		this.numberOfRows = numberOfRows;
		this.numberOfCols = numberOfCols;
		this.data = new int[numberOfRows][numberOfCols];
	}

	public int getNumberOfRows() {
		return numberOfRows;
	}

	public int getNumberOfCols() {
		return numberOfCols;
	}

	public void setElement(int row, int col, int value) {
		data[row][col] = value;
	}
	
	public int getElement(int row, int col) {
		return data[row][col];
	}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
