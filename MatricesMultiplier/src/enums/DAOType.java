package enums;

/**
 * Created by Sanny on 14.11.2014.
 */
public enum DAOType {
    IO, NIO, DB;
}
