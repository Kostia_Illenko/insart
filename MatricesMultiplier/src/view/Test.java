package view;

import dao.MatrixDAO;
import dao.factory.MatrixDAOFactory;
import enums.DAOType;

public class Test {
	public static void main(String []args) throws InterruptedException {
        

		MatrixDAO matrixDAO = new MatrixDAOFactory().getDAO(DAOType.DB);
		
		matrixDAO.deleteMatrixByName("matrix2");
	}
}
