<%@ attribute name="matrix" required="true" type="entity.Matrix"
	description="Matrix for showing"%>

<table class="table table-bordered">
	<tbody>
		<%
			for (int i = 0; i < matrix.getNumberOfRows(); i++) {
		%>
		<tr>
			<%
				for (int j = 0; j < matrix.getNumberOfRows(); j++) {
			%>
			<td><%=matrix.getElement(i, j)%></td>
			<%
				}
			%>
		</tr>
		<%
			}
		%>

	</tbody>
</table>