<%@page import="entity.Matrix"%>
<%@page import="entity.MatrixHistory"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="k" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<title>Show History</title>
</head>
<body>
	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header" style="font-family: Calibri">
			<a class="navbar-brand" href="/MatricesMultiplier/index.jsp">MatricesMultiplier</a>
		</div>
		<div>
			<ul class="nav navbar-nav" style="font-family: Calibri">
				<li><a href="/MatricesMultiplier/ControllerServlet?par=mat1">Matrix1</a></li>
				<li><a href="/MatricesMultiplier/ControllerServlet?par=mat2">Matrix2</a></li>
				<li class="active"><a href="/MatricesMultiplier/ControllerServlet?par=his">History</a></li>
			</ul>
		</div>
	</div>
	</nav><div class="container">
		<h2>${requestScope.matrixhistory.getDate()}</h2>
	</div>
	<div class="container">
		<h2>First Matrix</h2>
			<k:showMatrix matrix="${requestScope.matrixhistory.getMatrixFirst()}"></k:showMatrix>
	</div>
	<div class="container">
		<h2>Second Matrix</h2>
			<k:showMatrix matrix="${requestScope.matrixhistory.getMatrixSecond()}"></k:showMatrix>
	</div>
	<div class="container">
		<h2>Result Matrix</h2>
			<k:showMatrix matrix="${requestScope.matrixhistory.getMatrixResult()}"></k:showMatrix>
	</div>
	
</body>
</html>