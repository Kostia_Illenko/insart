package dao;

import java.util.List;

import entity.MatrixHistory;

public interface MatrixHistoryDAO extends DAO<MatrixHistory> {
	public List<MatrixHistory> getHistory();
}	
