package dao.db;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class AbstractDatabaseDAO {
	
	private static final String USENAME = "root";
	private static final String PASSWORD = "24091996";
	private static final String CONN_STRING = "jdbc:mysql://localhost/insartdb";
	
	protected Connection conn = null;
	
	public boolean openConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		if(conn == null) {
			try {
				conn = DriverManager.getConnection(CONN_STRING, USENAME, PASSWORD);
			} catch (SQLException e) {
				e.printStackTrace();
			
			}
		}
		return true;
	}
	
	public void close() { 
		try {
			conn.close();
		} catch (SQLException e) {
			System.out.println("Close problem");
		}
		conn = null;
	}
}
