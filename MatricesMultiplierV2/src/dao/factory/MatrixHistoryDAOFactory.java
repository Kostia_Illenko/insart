package dao.factory;

import dao.MatrixHistoryDAO;
import daoimpl.db.DatabaseMatrixHistoryDAOImpl;

public class MatrixHistoryDAOFactory extends AbstractFactory<MatrixHistoryDAO> {

	@Override
	MatrixHistoryDAO getIO() {
		return null;
	}

	@Override
	MatrixHistoryDAO getNIO() {
		return null;
	}

	@Override
	MatrixHistoryDAO getDB() {
		return new DatabaseMatrixHistoryDAOImpl();
	}
    
}
