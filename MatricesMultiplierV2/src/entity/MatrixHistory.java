package entity;

import java.util.Date;



public class MatrixHistory {
    private int id;
	private Date date;
	private Matrix matrixFirst;
	private Matrix matrixSecond;
	private Matrix matrixResult;
	
	public MatrixHistory(Date date, Matrix matrixFirst, Matrix matrixSecond,
			Matrix matrixResult) {
		super();
		this.id = 0;
		this.date = date;
		this.matrixFirst = matrixFirst;
		this.matrixSecond = matrixSecond;
		this.matrixResult = matrixResult;
	}
	
	public MatrixHistory(int id, Date date, Matrix matrixFirst, Matrix matrixSecond,
			Matrix matrixResult) {
		super();
		this.id = id;
		this.date = date;
		this.matrixFirst = matrixFirst;
		this.matrixSecond = matrixSecond;
		this.matrixResult = matrixResult;
	}

	public MatrixHistory(int id, Date date) {
		super();
		this.id = id;
		this.date = date;
		this.matrixFirst = new Matrix(1, 1);
		this.matrixSecond = new Matrix(1, 1);
		this.matrixResult = new Matrix(1, 1);
	}
	
	
	public Matrix getMatrixFirst() {
		return matrixFirst;
	}

	public void setMatrixFirst(Matrix matrixFirst) {
		this.matrixFirst = matrixFirst;
	}

	public Matrix getMatrixSecond() {
		return matrixSecond;
	}

	public void setMatrixSecond(Matrix matrixSecond) {
		this.matrixSecond = matrixSecond;
	}

	public Matrix getMatrixResult() {
		return matrixResult;
	}

	public void setMatrixResult(Matrix matrixResult) {
		this.matrixResult = matrixResult;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}	
}
