package service;

import java.util.Queue;

import entity.DB;

class Interval {
	public int startOfInterval;
	public int endOfInterval;
	
    public Interval(int startOfInterval, int endOfInterval) {
			this.startOfInterval = startOfInterval;
			this.endOfInterval = endOfInterval;
	}
}

public class Manager extends Thread {
	private Queue<Interval> queue;
	private Thread[] multiplyThreads;
    private volatile boolean done = false;
    
    public class MultiplyRunnableCustom implements Runnable {  	
   		@Override
        public void run() {
            while(!queue.isEmpty()) {
            	try {
	            	done = false;
	            	Interval interval = queue.poll();
			        int startOfInterval = interval.startOfInterval;
			        int endOfInterval = interval.endOfInterval;
			        for(int k  = startOfInterval; k <= endOfInterval; k++) {
			        	for(int i = 0; i < DB.getMatrix1().getNumberOfRows(); i++) {
			        		int value = 0;
			                for(int j = 0; j < DB.getMatrix2().getNumberOfCols(); j++) {
			                	value += (DB.matrix1.getElement(k, j) * DB.matrix2.getElement(j, i));
			                }
			                DB.out.setElement(k, i, value);
			            }
			        }
            	}
            	catch(java.lang.ArrayIndexOutOfBoundsException e) {
            		break;
            	}
            }
   		}
    }
    
	public Manager(int numberOfThreads, Queue<Interval> queue, int part) {
   		this.multiplyThreads = new Thread[numberOfThreads];
        this.queue = queue;
	}

	@Override
	public void run() {
        for (int i = 0; i < multiplyThreads.length; i++) {
			multiplyThreads[i] = new Thread(new MultiplyRunnableCustom());
			multiplyThreads[i].start();	
		}
       
        for (int i = 0; i < multiplyThreads.length; i++) {
            if (multiplyThreads[i].isAlive()) {
            	try {
            		multiplyThreads[i].join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
           done = true;
	}

    public boolean isDone() {
        return done;
    }
}
