package dao;

import entity.Matrix;

public interface DAO<T> {
	 void write(T entity);
	 T read(int id);
	 void update(T entity);
	 void delete(int id);
}
