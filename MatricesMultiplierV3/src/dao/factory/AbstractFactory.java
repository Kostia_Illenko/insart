package dao.factory;

import enums.DAOType;

/**
 * Created by Sanny on 14.11.2014.
 */
public abstract class AbstractFactory<T> {
    public T getDAO(DAOType type) {
        switch (type) {
            case IO: return getIO();
            case NIO: return getNIO();
            case DB: return getDB();
            default: return null;
        }
    }

    abstract T getIO();
    abstract T getNIO();
    abstract T getDB();
}
