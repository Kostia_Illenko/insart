package jstl;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import entity.Matrix;

public class ShowMatrixTag extends TagSupport{
	private Matrix matrix;

	public void setMatrix(Matrix matrix) {
		this.matrix = matrix;
	}
    
	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
		try {
				out.print("table class=\"table table-bordered\"><tbody>");
			
			for(int i = 0; i < matrix.getNumberOfRows(); i++) {
				out.print("<tr>");
				for(int j = 0; j < matrix.getNumberOfCols(); j++) {
					out.print("<td>" + matrix.getElement(i, j) + " " + "</td>");
				}
				out.print("</tr>");
			}

			out.print("</tbody></table>");
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
        return SKIP_BODY;
    }
    
    
    public int doEndTag() throws JspException {
        return SKIP_PAGE;
    }
}
