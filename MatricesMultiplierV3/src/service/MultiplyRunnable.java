package service;

import entity.DB;

public class MultiplyRunnable implements Runnable {
	private volatile int row;
	public MultiplyRunnable(int row) {
		this.row = row;
	}
	
	@Override
	public void run() {	
		for(int i = 0; i < DB.getMatrix1().getNumberOfRows(); i++) {
			int value = 0;
			for(int j = 0; j < DB.getMatrix2().getNumberOfCols(); j++) {
		    	value += (DB.getMatrix1().getElement(row, j) * DB.getMatrix2().getElement(j, i));
			}
			DB.out.setElement(row, i, value);
		}
	}
}
