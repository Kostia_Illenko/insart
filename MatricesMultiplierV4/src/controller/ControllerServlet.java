package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import service.Service;
import entity.Matrix;
import entity.MatrixHistory;
import enums.DAOType;

/**
 * Servlet implementation class ControllerServlet
 */
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public final static String GET_MATRIX1 = "mat1";
	public final static String GET_MATRIX2 = "mat2";
	public final static String GET_MATRIX_RESULT = "res";
	public final static String GET_HISTORY = "his";
	public final static String UPDATE_MATRIX = "ed";
	public final static String SHOW_HISTORY = "shis";
	public final static String DELETE_HISTORY = "dhis";
	public final static String MULTIPLE = "mult";
	
	public static Logger logger = null;
	
	  /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerServlet() {
        super();
    }
    
    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
    	logger = Logger.getRootLogger();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String param = request.getParameter("par");
		Service service = new Service(DAOType.DB);
		
		request.setAttribute("par", param);
		
		switch (param) {
			case GET_MATRIX1: {
				logger.info("Get matrix 1");
				Matrix matrix = service.readMatrix("matrix1");
				request.setAttribute("matrix", matrix);
				request.getRequestDispatcher("/matrix.jsp").forward(request, response);
			}
				break;
			case GET_MATRIX2: {
				logger.info("Get matrix 2");
				request.setAttribute("matrix", service.readMatrix("matrix2"));
				request.getRequestDispatcher("/matrix.jsp").forward(request, response);
			}
				break;
			case UPDATE_MATRIX: {
				String mat = (String) request.getParameter("mat");
				String matrixName;
				if(mat.equals(GET_MATRIX1)) {
					matrixName = "matrix1";
				} else { 
					matrixName = "matrix2";
				}
				logger.info("Update matrix " + matrixName);
				int numberOfRows = Integer.parseInt((String)request.getParameter("numberOfRows"));
				int numberOfCols = Integer.parseInt((String)request.getParameter("numberOfCols"));
				int counter = 0;
				for(int i = 0; i < numberOfRows; i++) {
					for(int j = 0; j < numberOfCols; j++) {
						service.updateMatrixByXandY(matrixName, i, j, Integer.parseInt((String)request.getParameter("c" + counter)));
						counter++;
					}
				}
				request.setAttribute("matrix", service.readMatrix(matrixName));
				request.getRequestDispatcher("/matrix.jsp").forward(request, response);
			}
				break;
			case GET_HISTORY: {
				logger.info("Get history");
				List<MatrixHistory> histories = service.getHistory();
				request.setAttribute("histories", histories);
				request.getRequestDispatcher("/history.jsp").forward(request, response);
			}
				break;
			case SHOW_HISTORY: {
				int id = Integer.parseInt((String)request.getParameter("id"));
				logger.info("Show history with id " + id);
				MatrixHistory history = service.getMatrixHistory(id);
				request.setAttribute("matrixhistory", history);
				request.getRequestDispatcher("/show-history.jsp").forward(request, response);
			}
				break;
			case DELETE_HISTORY: {
				int idd = Integer.parseInt((String)request.getParameter("id"));
				logger.info("Delete history with id " + idd);
				service.deleteMatrixHistory(idd);
				List<MatrixHistory> histories = service.getHistory();
				request.setAttribute("histories", histories);
				request.getRequestDispatcher("/history.jsp").forward(request, response);
			}
				break;
			case MULTIPLE: {
				String mat = (String) request.getParameter("mat");
				String matrixName;
				String matrixSecondName;
				
				if(mat.equals(GET_MATRIX1)) {
					matrixName = "matrix1";
					matrixSecondName = "matrix2";
				} else { 
					matrixName = "matrix2";
					matrixSecondName = "matrix1";
				}
				int numberOfRows = Integer.parseInt((String)request.getParameter("numberOfRows"));
				int numberOfCols = Integer.parseInt((String)request.getParameter("numberOfCols"));
				int counter = 0;
				for(int i = 0; i < numberOfRows; i++) {
					for(int j = 0; j < numberOfCols; j++) {
						service.updateMatrixByXandY(matrixName, i, j, Integer.parseInt((String)request.getParameter("c" + counter)));
						counter++;
					}
				}
				logger.info(matrixName + "x" + matrixSecondName);
				Matrix matrix1 = service.readMatrix(matrixName);
				Matrix matrix2 = service.readMatrix(matrixSecondName);
				service.calculate(matrix1, matrix2);
				List<MatrixHistory> histories = service.getHistory();
				MatrixHistory history = service.getMatrixHistory(histories.get(histories.size()-1).getId());
				request.setAttribute("matrixhistory", history);
				request.getRequestDispatcher("/show-history.jsp").forward(request, response);
			}
			default:
				break;
		}
	}

}
