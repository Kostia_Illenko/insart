package dao;

import entity.Matrix;

/**
 * Created by Sanny on 14.11.2014.
 */
public interface MatrixDAO extends DAO<Matrix> {
    void updateMatrixByXandY(String name,int x, int y, int value);
    Matrix getMatrixByName(String name);
    void deleteMatrixByName(String name);
}
