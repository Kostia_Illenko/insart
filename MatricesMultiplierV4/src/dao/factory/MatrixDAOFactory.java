package dao.factory;

import dao.MatrixDAO;
import daoimpl.db.DatabaseMatrixDAOImpl;
import daoimpl.io.IOMatrixDAOImpl;
import daoimpl.io.NIOMatrixDAOImpl;


public class MatrixDAOFactory extends AbstractFactory<MatrixDAO> {

    @Override
    MatrixDAO getIO() {
        return new IOMatrixDAOImpl();
    }

    @Override
    MatrixDAO getNIO() {
        return new NIOMatrixDAOImpl();
    }

    @Override
    MatrixDAO getDB() {
        return new DatabaseMatrixDAOImpl();
    }
}