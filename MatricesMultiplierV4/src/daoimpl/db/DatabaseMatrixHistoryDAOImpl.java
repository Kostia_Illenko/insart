package daoimpl.db;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import controller.ControllerServlet;
import dao.MatrixHistoryDAO;
import dao.db.AbstractDatabaseDAO;
import entity.Matrix;
import entity.MatrixHistory;

public class DatabaseMatrixHistoryDAOImpl extends AbstractDatabaseDAO implements MatrixHistoryDAO {


    @Override
	public void write(MatrixHistory matrixHistory) {
		openConnection();
		ControllerServlet.logger.info("Write matrixhistory to DB");
		String sql = "INSERT INTO matrix_history (date, matrix_first, matrix_second, matrix_result) " +
				"VALUES (?, ?, ?, ?)";
		
		long time = matrixHistory.getDate().getTime();
		Timestamp timestamp = new Timestamp(time);
	
			PreparedStatement stmt = null;
			try {
				stmt = conn.prepareStatement(sql);
				stmt.setTimestamp(1, timestamp);
				stmt.setObject(2, matrixHistory.getMatrixFirst());
				stmt.setObject(3, matrixHistory.getMatrixSecond());
				stmt.setObject(4, matrixHistory.getMatrixResult());
				stmt.executeUpdate();

				stmt.close();
			}
			catch (SQLException e) {
				ControllerServlet.logger.error("Write error");
			}
	}


    @Override
    public MatrixHistory read(int id) {
    	ControllerServlet.logger.info("Get matrixhistory with id "  + id);
    	String sql = "SELECT date, matrix_first, matrix_second, matrix_result FROM matrix_history WHERE id=?";
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
			openConnection();
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
		}
		catch(SQLException e) {
			ControllerServlet.logger.error("Read error");
		}
		
		MatrixHistory history = null;
		try {
			while(rs.next()) {
				try {
					history = new MatrixHistory(new Date(rs.getTimestamp("date").getTime()), 
							(Matrix) new ObjectInputStream(new ByteArrayInputStream(rs.getBytes("matrix_first"))).readObject(), 
							(Matrix) new ObjectInputStream(new ByteArrayInputStream(rs.getBytes("matrix_second"))).readObject(),
							(Matrix) new ObjectInputStream(new ByteArrayInputStream(rs.getBytes("matrix_result"))).readObject());
				} catch (ClassNotFoundException | SQLException | IOException e) {
					ControllerServlet.logger.error("Read error");
				}
			}
		} catch (SQLException e) {
			ControllerServlet.logger.error("Read error");
		}		
		return history;
    }

    @Override
    public void update(MatrixHistory entity) {
    	throw new UnsupportedOperationException("That is the unsopported operation for work with matrix's history");
    }

    @Override
    public void delete(int id) {
		openConnection();
		ControllerServlet.logger.info("Delete matrixhistory with id "  + id);
		String sql = "DELETE FROM matrix_history WHERE id = ?";
		try {
			PreparedStatement stmt = conn.prepareStatement(sql);
			
			stmt.setInt(1, id);
			stmt.executeUpdate();

			stmt.close();
		} catch (SQLException e) {
			ControllerServlet.logger.error("Delete error");
		}
	}


	@Override
	public List<MatrixHistory> getHistory() {
		String sql = "SELECT id, date FROM matrix_history";
		ResultSet rs = null;
		PreparedStatement stmt = null;
		List<MatrixHistory> histories = new ArrayList<>();
		ControllerServlet.logger.info("Get matrixhistory");
		
		try {
			openConnection();
			stmt = conn.prepareStatement(sql);
			rs = stmt.executeQuery();			
		}
		catch(SQLException e) {
			ControllerServlet.logger.error("Read error");
		}
		try {
			while(rs.next()) {
				histories.add(new MatrixHistory(rs.getInt("id"), new Date(rs.getTimestamp("date").getTime())));
			}

			stmt.close();
		} catch (SQLException e) {
			ControllerServlet.logger.error("Read error");
		}

		return histories;
	}	
}