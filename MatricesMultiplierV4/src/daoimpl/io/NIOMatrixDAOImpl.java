package daoimpl.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import dao.DAO;
import dao.MatrixDAO;
import entity.Matrix;

public class NIOMatrixDAOImpl implements MatrixDAO {

	@Override
	public void write(Matrix matrix) {
		File f = new File(matrix.getName()+"_nio.matrix");
	    FileChannel channel;
		try {
			channel = new FileOutputStream(f).getChannel();
			ByteArrayOutputStream baos = new ByteArrayOutputStream();  
			ObjectOutputStream oos = new ObjectOutputStream (baos);  
			oos.writeObject (matrix);  
			oos.flush();  
			channel.write (ByteBuffer.wrap (baos.toByteArray()));  
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


    @Override
    public Matrix read(int id) {
		 throw new UnsupportedOperationException("That is the unsopported operation for work with IO package");
    }

    @Override
    public void update(Matrix entity) {
		 throw new UnsupportedOperationException("That is the unsopported operation for work with IO package");
	}

    @Override
	public void delete(int id) {
		 throw new UnsupportedOperationException("That is the unsopported operation for work with IO package");
	}

    @Override
    public void updateMatrixByXandY(String name, int x, int y, int value) { 
		 throw new UnsupportedOperationException("That is the unsopported operation for work with IO package");
	}

    @Override
    public Matrix getMatrixByName(String name) {
        RandomAccessFile aFile;
        Matrix matrix = null;

        try {
            aFile = new RandomAccessFile(name+"_nio.matrix", "r");

            FileChannel inChannel = aFile.getChannel();
            MappedByteBuffer buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, 0, inChannel.size());
            buffer.load();
            byte[] bytearray = new byte[buffer.limit()];

            for(int i = 0; i < buffer.limit(); i++) {
                bytearray[i] = buffer.get(i);
            }

            ByteArrayInputStream is = new ByteArrayInputStream(bytearray);
            ObjectInputStream ois =   new ObjectInputStream(is);

            matrix = (Matrix) ois.readObject();
            buffer.clear();
            inChannel.close();
            aFile.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return matrix;
    }


	@Override
	public void deleteMatrixByName(String name) {
		 throw new UnsupportedOperationException("That is the unsopported operation for work with IO package");
	}
}
