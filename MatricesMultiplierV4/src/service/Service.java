package service;

import java.util.Date;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;



import controller.ControllerServlet;
import dao.MatrixDAO;
import dao.MatrixHistoryDAO;
import dao.factory.MatrixDAOFactory;
import dao.factory.MatrixHistoryDAOFactory;
import entity.DB;
import entity.Matrix;
import entity.MatrixHistory;
import enums.DAOType;

public class Service {
	private MatrixDAO matrixDAO;
	private MatrixHistoryDAO matrixHistoryDAO;
	
	public Service(DAOType type) {
        matrixDAO = new MatrixDAOFactory().getDAO(type);
        matrixHistoryDAO =  new MatrixHistoryDAOFactory().getDAO(DAOType.DB);
	}

	public void calculate(Matrix matrix1, Matrix matrix2) {

		long time = new Date().getTime();
		
		
		Queue<Interval> multiplyRunnables = new ConcurrentLinkedQueue<>();
		
		DB.setMatrix1(matrix1);
		DB.setMatrix2(matrix2);
		DB.setOut(new Matrix(matrix1.getNumberOfRows(), matrix2.getNumberOfCols()));


		int numberOfThreads = 1;
		int matsize = matrix1.getNumberOfRows();
		int part = DB.matrix1.getNumberOfRows();
		  
		if(matsize > 500 && matsize <= 1000) {
		   numberOfThreads = 8;
		   part = 16;
		}
		if(matsize > 1000 && matsize <= 5000) {
			numberOfThreads = 16;
		    part = DB.matrix1.getNumberOfRows() / (numberOfThreads*32);
		}
		if(matsize > 5000 && matsize <= 10000) {
			numberOfThreads = 32;
		    part = DB.matrix1.getNumberOfRows() / (numberOfThreads*32);
		}
		
        for (int i = 1; i < DB.matrix1.getNumberOfRows()-1; i += (part)) {
            	multiplyRunnables.add(new Interval(i-1, i+part-2));
        }
		
		Manager manager = new Manager(numberOfThreads,multiplyRunnables, part);
		
		manager.start();

		while(!manager.isDone()) { }

		ControllerServlet.logger.info("Matrixes is multiplied.");
		ControllerServlet.logger.info("Time: " + (double) (new Date().getTime() - time) / 1000 + "s.");
		
		MatrixHistory matrixHistory = new MatrixHistory(new Date(), DB.getMatrix1(), DB.getMatrix2(), DB.getOut()); 
	
		
        matrixHistoryDAO.write(matrixHistory);
        
        showMatrix(DB.out);
	}
	
	public void writeMatrix(Matrix matrix) {
        matrixDAO.write(matrix);
	}
	
	public Matrix readMatrix(String nameOfFile) {
		return (Matrix) matrixDAO.getMatrixByName(nameOfFile);
	}
	
	public Matrix generateMatrix(int numberOfRows, int numberOfCols) {
		Random random = new Random();
		Matrix matrix = new Matrix(numberOfRows, numberOfCols);
		int rnd;
		for(int i = 0; i < numberOfRows; i++) {
			for(int j = 0; j < numberOfCols; j++) {
				rnd = random.nextInt() % 10;
				matrix.setElement(i, j, rnd);
			}
		}
		return matrix;
	}
	
	public static void showMatrix(Matrix matrix) {
		for(int i = 0; i < matrix.getNumberOfRows(); i++) {
			for(int j = 0; j < matrix.getNumberOfCols(); j++) {
				System.out.print(matrix.getElement(i, j) + " ");
			}
			System.out.println();
		}
	}

	public void updateMatrixByXandY(String name, int x, int y, int value) {
		matrixDAO.updateMatrixByXandY(name, x, y, value);
	}
	
	public List<MatrixHistory> getHistory() {
		return matrixHistoryDAO.getHistory();
	}
	
	public MatrixHistory getMatrixHistory(int id) {
		return matrixHistoryDAO.read(id);
	}
	
	public void deleteMatrixHistory(int id) {
		matrixHistoryDAO.delete(id);
	}
}
