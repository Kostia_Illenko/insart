<%@page import="com.illenko.entity.MatrixHistory"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<title>History</title>

</head>
<body>
	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header" style="font-family: Calibri">
			<a class="navbar-brand" href="<%=request.getContextPath()%>/index.jsp">MatricesMultiplier</a>
		</div>
		<div>
			<ul class="nav navbar-nav" style="font-family: Calibri">
				<li><a href="<%=request.getContextPath()%>/matrix-view/matrix1">Matrix1</a></li>
				<li><a href="<%=request.getContextPath()%>/matrix-view/matrix2">Matrix2</a></li>
				<li class="active"><a
					href="<%=request.getContextPath()%>/history">History</a></li>
			</ul>
		</div>
	</div>
	</nav>
	<div class="container">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>ID</th>
					<th>Date</th>
					<th>Options</th>
				</tr>
			</thead>
			<tbody>

				<c:forEach var="hist" items="${requestScope['histories']}">
					<tr>
						<td>${hist.getId()}</td>
						<td>${hist.getDate()}</td>
						<td><a
							href="<%=request.getContextPath()%>/view-history/${hist.getId()}"
							class="btn btn-default btn-sm"> <span
								class="glyphicon glyphicon-eye-open"></span> View
						</a> <a
							href="<%=request.getContextPath()%>/delete-history/${hist.getId()}"
							class="btn btn-default btn-sm"> <span
								class="glyphicon glyphicon-remove"></span> Remove
						</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>