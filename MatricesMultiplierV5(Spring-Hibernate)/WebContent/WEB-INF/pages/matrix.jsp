<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.illenko.entity.Matrix"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="k" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>MatricesMultiplier</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

</head>
<body>
	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header" style="font-family: Calibri">
			<a class="navbar-brand" href="<%=request.getContextPath()%>/index.jsp">MatricesMultiplier</a>
		</div>
		<div>
			<ul class="nav navbar-nav" style="font-family: Calibri">
				<li <c:if test="${requestScope.par == 'matrix1'}"> class="active"</c:if>><a href="<%=request.getContextPath()%>/matrix-view/matrix1">Matrix1</a></li>
				<li <c:if test="${requestScope.par == 'matrix2'}"> class="active"</c:if>><a href="<%=request.getContextPath()%>/matrix-view/matrix2">Matrix2</a></li>
				<li><a href="<%=request.getContextPath()%>/history">History</a></li>
			</ul>
		</div>
	</div>
	</nav>

	<div class="container">
		<form role="form" action="<%=request.getContextPath()%>/matrix-edit/${par}"
			method="post">
			<k:showEditableMatrix matrix="${requestScope['matrix']}"></k:showEditableMatrix>
			<input type="hidden" value="${requestScope['matrix'].getNumberOfRows()}" name="numberOfRows">
			<input type="hidden" value="${requestScope['matrix'].getNumberOfCols()}" name="numberOfCols"> 
			<input type="hidden" value="${requestScope.par}" name="mat">
			<button type="submit" value="ed" name="par" class="btn btn-default">Save</button>
			<button type="submit" value="mult" name="par" class="btn btn-default" >Multiple</button>
			
		</form>
	</div>
</body>
</html>