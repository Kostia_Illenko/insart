<%@ attribute name="matrix" required="true" type="com.illenko.entity.Matrix"
	description="Matrix for edit"%>

<table class="table table-bordered">
	<tbody>
		<%!int idOf = 0;%>
		<%
			for (int i = 0; i < matrix.getNumberOfRows(); i++) {
		%>
		<tr>
			<%
				for (int j = 0; j < matrix.getNumberOfCols(); j++) {
			%>
			<td style="text-align: center">
				<div class="form-group">
					<input type="number" class="form-control"
						name="<%=new String("c" + idOf)%>"
						value="<%=matrix.getElement(i, j)%>">
				</div>
			</td>
			<%
				idOf++;
					}
			%>
		</tr>
		<%
			}
			idOf = 0;
		%>
	</tbody>
</table> 