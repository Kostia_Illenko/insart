package com.illenko.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.illenko.entity.MatrixHistory;
import com.illenko.enums.DAOType;
import com.illenko.service.Service;

@Controller
public class HistoryController {

	private static Service service = new Service(DAOType.DB);
	
	@RequestMapping("/history")
	public ModelAndView displayAllHistory() {
		List<MatrixHistory> histories = service.getHistory();
		ModelAndView model = new ModelAndView("history");
		model.addObject("par", "history");
		model.addObject("histories", histories);
		return model;
	}
	
	@RequestMapping("/delete-history/{id}")
	public ModelAndView deleteHistory(@PathVariable int id) {
		service.deleteMatrixHistory(id);
		List<MatrixHistory> histories = service.getHistory();
		ModelAndView model = new ModelAndView("history");
		model.addObject("par", "history");
		model.addObject("histories", histories);
		return model;
	}
	
	@RequestMapping("/view-history/{id}")
	public ModelAndView viewHistory(@PathVariable int id) {
		MatrixHistory history = service.getMatrixHistory(id);
		ModelAndView model = new ModelAndView("show-history");
		model.addObject("matrixhistory", history);
		return model;
	}
	
}