package com.illenko.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.illenko.entity.Matrix;
import com.illenko.entity.MatrixHistory;
import com.illenko.enums.DAOType;
import com.illenko.service.Service;

@Controller
public class MatrixViewController {


	private static Service service = new Service(DAOType.DB);
	
	public final static String UPDATE_MATRIX = "ed";
	public final static String MULTIPLE = "mult";

	
	
	@RequestMapping("/matrix-view/{matrixname}")
	public ModelAndView viewMatrix(@PathVariable String matrixname) {
		ModelAndView model = new ModelAndView("matrix");
		Matrix matrix = service.readMatrix(matrixname);
		model.addObject("par", matrixname);
		model.addObject("matrix", matrix);
		return model;
	}

	@RequestMapping("/matrix-edit/{matrixname}")
	public ModelAndView saveOrMultiply(@PathVariable String matrixname,
			HttpServletRequest request, HttpServletResponse response) {


		if (request.getParameter("par").equals(UPDATE_MATRIX)) {
			int numberOfRows = Integer.parseInt((String) request.getParameter("numberOfRows"));
			int numberOfCols = Integer.parseInt((String) request.getParameter("numberOfCols"));
			int counter = 0;
			for (int i = 0; i < numberOfRows; i++) {
				for (int j = 0; j < numberOfCols; j++) {
					service.updateMatrixByXandY(matrixname,	i, j, Integer.parseInt((String) request.getParameter("c" + counter)));
					counter++;
				}
			}

			String string = request.getParameter("par");
			ModelAndView model = new ModelAndView("matrix");
			Matrix matrix = service.readMatrix(matrixname);
			model.addObject("matrix", matrix);
			model.addObject("par", matrixname);
			return model;
		} else {
			String matrixName;
			String matrixSecondName;

			if (matrixname.equals("matrix1")) {
				matrixName = "matrix1";
				matrixSecondName = "matrix2";
			} else {
				matrixName = "matrix2";
				matrixSecondName = "matrix1";
			}
			int numberOfRows = Integer.parseInt((String) request.getParameter("numberOfRows"));
			int numberOfCols = Integer.parseInt((String) request.getParameter("numberOfCols"));
			int counter = 0;
			for (int i = 0; i < numberOfRows; i++) {
				for (int j = 0; j < numberOfCols; j++) {
					service.updateMatrixByXandY(matrixName, i, j, Integer.parseInt((String) request.getParameter("c" + counter)));
					counter++;
				}
			}
			Matrix matrix1 = service.readMatrix(matrixName);
			Matrix matrix2 = service.readMatrix(matrixSecondName);
			service.calculate(matrix1, matrix2);
			List<MatrixHistory> histories = service.getHistory();
			MatrixHistory history = service.getMatrixHistory(histories.get(histories.size() - 1).getId());
			ModelAndView modelAndView = new ModelAndView("show-history");
			modelAndView.addObject("matrixhistory", history);
			return modelAndView;
		}

		
		
		
	}
}