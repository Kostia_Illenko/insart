package com.illenko.dao;

import java.util.List;

import com.illenko.entity.MatrixHistory;

public interface MatrixHistoryDAO extends DAO<MatrixHistory> {
	public List<MatrixHistory> getHistory();
}	
