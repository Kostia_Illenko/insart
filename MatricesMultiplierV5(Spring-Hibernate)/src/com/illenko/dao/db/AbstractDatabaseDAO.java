package com.illenko.dao.db;


import org.hibernate.Session;

import com.illenko.utils.HibernateUtil;

public abstract class AbstractDatabaseDAO {
	
	protected Session session = null;
	
	public void openConnection() {
		session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	}
	
	public void close() { 
		session.getTransaction().commit();
		if (session != null && session.isOpen()) {
			session.close();
		}
	}
}
