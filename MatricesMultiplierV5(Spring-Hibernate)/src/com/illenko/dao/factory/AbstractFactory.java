package com.illenko.dao.factory;

import com.illenko.enums.DAOType;

/**
 * Created by Sanny on 14.11.2014.
 */
public abstract class AbstractFactory<T> {
    public T getDAO(DAOType type) {
        switch (type) {
            case DB: return getDB();
            default: return null;
        }
    }

    abstract T getDB();
}
