package com.illenko.dao.factory;

import com.illenko.dao.MatrixDAO;
import com.illenko.daoimpl.db.DatabaseMatrixDAOImpl;


public class MatrixDAOFactory extends AbstractFactory<MatrixDAO> {


    @Override
    MatrixDAO getDB() {
        return new DatabaseMatrixDAOImpl();
    }
}