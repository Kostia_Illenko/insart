package com.illenko.dao.factory;

import com.illenko.dao.MatrixHistoryDAO;
import com.illenko.daoimpl.db.DatabaseMatrixHistoryDAOImpl;

public class MatrixHistoryDAOFactory extends AbstractFactory<MatrixHistoryDAO> {

	@Override
	MatrixHistoryDAO getDB() {
		return new DatabaseMatrixHistoryDAOImpl();
	}
    
}
