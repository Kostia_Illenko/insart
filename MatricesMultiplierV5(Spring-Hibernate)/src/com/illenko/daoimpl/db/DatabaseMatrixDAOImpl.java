package com.illenko.daoimpl.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.illenko.dao.MatrixDAO;
import com.illenko.dao.db.AbstractDatabaseDAO;
import com.illenko.entity.Matrix;
import com.illenko.utils.HibernateUtil;

public class DatabaseMatrixDAOImpl extends GenericDAOImpl<Matrix> implements
		MatrixDAO {

	@Override
	public void write(Matrix matrix) {
		String hql = "INSERT INTO matrix (matrix_name, row_id, col_id, value) VALUES (:matrix_name, :row_id, :col_id, :value)";

		for (int i = 0; i < matrix.getNumberOfRows(); i++) {
			for (int j = 0; j < matrix.getNumberOfCols(); j++) {
				openConnection();
				Query query = session.createSQLQuery(hql);
				query.setParameter("matrix_name", matrix.getName());
				query.setParameter("row_id", i);
				query.setParameter("col_id", j);
				query.setParameter("value", matrix.getElement(i, j));
				query.executeUpdate();
			}
		}

		session.getTransaction().commit();
	}

	@Override
	public void updateMatrixByXandY(String name, int x, int y, int value) {
		openConnection();
		String sql = "UPDATE `matrix` SET value = :value  WHERE matrix_name = :matrix_name AND row_id = :row_id AND col_id = :col_id";
		Query query = session.createSQLQuery(sql);
		query.setParameter("matrix_name", name);
		query.setParameter("row_id", x);
		query.setParameter("col_id", y);
		query.setParameter("value", value);
		query.executeUpdate();

		session.getTransaction().commit();
	}

	@Override
	public Matrix getMatrixByName(String name) {
		openConnection();
		String sql = "SELECT row_id, col_id, value FROM matrix WHERE matrix_name = :matrix_name";
		Query query = session.createSQLQuery(sql);
		query.setParameter("matrix_name", name);
		List<Object[]> list = query.list();
		int counter = 0;
		for (Object object[] : list) {
			counter++;
		}

		Matrix matrix = new Matrix((int) Math.sqrt((double) counter),
				(int) (Math.sqrt((double) counter)));
		for (Object object[] : list) {
			matrix.setElement(Integer.parseInt(object[0].toString()),
					Integer.parseInt(object[1].toString()),
					Integer.parseInt(object[2].toString()));
		}

		session.getTransaction().commit();
		return matrix;
	}

	@Override
	public void deleteMatrixByName(String name) {
		openConnection();
		String sql = "DELETE FROM matrix WHERE matrix_name = :matrix_name";
		Session session = null;
		session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query query = session.createSQLQuery(sql);
		query.setParameter("matrix_name", name);
		query.executeUpdate();	

		session.getTransaction().commit();
	}

}
