package com.illenko.daoimpl.db;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.illenko.dao.MatrixHistoryDAO;
import com.illenko.dao.db.AbstractDatabaseDAO;
import com.illenko.entity.Matrix;
import com.illenko.entity.MatrixHistory;
import com.illenko.utils.HibernateUtil;

public class DatabaseMatrixHistoryDAOImpl extends GenericDAOImpl<MatrixHistory> implements
		MatrixHistoryDAO {
	
	@Override
	public List<MatrixHistory> getHistory() {

		openConnection();
		List<MatrixHistory> histories = new ArrayList<MatrixHistory>();
		
			histories = session.createCriteria(MatrixHistory.class).list();
		
		return histories;
	}

}