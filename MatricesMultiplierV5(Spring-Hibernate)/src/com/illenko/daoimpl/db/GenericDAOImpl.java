package com.illenko.daoimpl.db;

import java.lang.reflect.ParameterizedType;

import org.hibernate.Session;

import com.illenko.dao.DAO;
import com.illenko.utils.HibernateUtil;

public abstract class GenericDAOImpl<T> implements DAO<T> {

	protected Session session = null;

	public void openConnection() {
			session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		
	}

	public void close() {
		if (session != null && session.isOpen()) {
			session.close();
		}
	}

	@SuppressWarnings({ "unchecked" })
	protected Class<T> getEntityClass() {
		ParameterizedType ptype = (ParameterizedType) getClass()
				.getGenericSuperclass();
		return (Class<T>) ptype.getActualTypeArguments()[0];
	}

	@Override
	public void write(T entity) {
		openConnection();
		session.save(entity);
		session.getTransaction().commit();
	}

	@Override
	public T read(int id) {
		openConnection();
		T entity = null;
		entity = (T) session.get(getEntityClass(), id);
		
		return entity;
	}

	@Override
	public void update(T entity) {
		openConnection();
		session.saveOrUpdate(entity);
		session.getTransaction().commit();
	}

	@Override
	public void delete(int id) {
		openConnection();
		T entity = null;
		entity = (T) session.get(getEntityClass(), id);
		session.delete(entity);
		session.getTransaction().commit();
	}

}
