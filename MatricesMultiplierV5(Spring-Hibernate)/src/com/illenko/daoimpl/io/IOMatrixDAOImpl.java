package com.illenko.daoimpl.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.illenko.dao.MatrixDAO;
import com.illenko.entity.Matrix;

public class IOMatrixDAOImpl implements MatrixDAO {

	@Override
	public void write(Matrix entity) {
		ObjectOutputStream out;
		try {
			out = new ObjectOutputStream(new FileOutputStream(entity.getName()
					+ "_io.matrix"));
			out.writeObject(entity);
			out.close();
		} catch (IOException e) {
			System.out.println("Write exception!");
		}
	}

	@Override
	public Matrix read(int id) {
		throw new UnsupportedOperationException(
				"That is the unsopported operation for work with IO package");
	}

	@Override
	public void update(Matrix entity) {
		throw new UnsupportedOperationException(
				"That is the unsopported operation for work with IO package");
	}

	@Override
	public void delete(int id) {
		throw new UnsupportedOperationException(
				"That is the unsopported operation for work with IO package");
	}

	@Override
	public void updateMatrixByXandY(String name, int x, int y, int value) {
		throw new UnsupportedOperationException(
				"That is the unsopported operation for work with IO package");
	}

	@Override
	public Matrix getMatrixByName(String name) {
		ObjectInputStream in;
		Matrix matrix = null;
		try {
			in = new ObjectInputStream(new FileInputStream(name + "_io.matrix"));
			matrix = (Matrix) in.readObject();
			in.close();
		} catch (IOException e) {
			System.out.println("Read exception!");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return matrix;
	}

	@Override
	public void deleteMatrixByName(String name) {
		throw new UnsupportedOperationException(
				"That is the unsopported operation for work with IO package");
	}

}
