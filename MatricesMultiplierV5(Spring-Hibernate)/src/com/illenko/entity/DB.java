package com.illenko.entity;

public class DB {
    public static Matrix matrix1;
    public static Matrix matrix2;
	public static volatile Matrix out;
	public static Matrix getMatrix1() {
		return matrix1;
	}
		
	public static void setMatrix1(Matrix matrix1) {
		DB.matrix1 = matrix1;
	}
	public static Matrix getMatrix2() {
		return matrix2;
	}
	public static void setMatrix2(Matrix matrix2) {
		DB.matrix2 = matrix2;
	}

	public static void setOut(Matrix out) {
		DB.out = out;
	}

	public static Matrix getOut() {
		return out;
	}
	
}
