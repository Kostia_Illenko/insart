package com.illenko.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;





@Entity
@Table(name = Matrix.TABLE_NAME)
public class Matrix implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8282763129985897953L;
	public static final String TABLE_NAME = "matrix";
	private int id;
	private int numberOfRows;
	private int numberOfCols;
	private int[][] data;
	private String name;
	
	
	@Id
	@Column(name="id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int[][] getData() {
		return data;
	}

	public void setData(int[][] data) {
		this.data = data;
	}

	public void setNumberOfRows(int numberOfRows) {
		this.numberOfRows = numberOfRows;
	}

	public void setNumberOfCols(int numberOfCols) {
		this.numberOfCols = numberOfCols;
	}

	public Matrix(int numberOfRows, int numberOfCols) {
		super();
		this.numberOfRows = numberOfRows;
		this.numberOfCols = numberOfCols;
		this.data = new int[numberOfRows][numberOfCols];
	}

	public Matrix() {
	}

	public int getNumberOfRows() {
		return numberOfRows;
	}

	public int getNumberOfCols() {
		return numberOfCols;
	}

	public void setElement(int row, int col, int value) {
		data[row][col] = value;
	}

	public int getElement(int row, int col) {
		return data[row][col];
	}

	
	@Column(name="matrix_name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
