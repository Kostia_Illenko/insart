package com.illenko.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = MatrixHistory.TABLE_NAME)
public class MatrixHistory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1780836686259267330L;

	public static final String TABLE_NAME = "matrix_history";

	private int id;
	private Date date;
	private Matrix matrixFirst;
	private Matrix matrixSecond;
	private Matrix matrixResult;

	public MatrixHistory(Date date, Matrix matrixFirst, Matrix matrixSecond,
			Matrix matrixResult) {
		super();
		this.id = 0;
		this.date = date;
		this.matrixFirst = matrixFirst;
		this.matrixSecond = matrixSecond;
		this.matrixResult = matrixResult;
	}

	public MatrixHistory(int id, Date date, Matrix matrixFirst,
			Matrix matrixSecond, Matrix matrixResult) {
		super();
		this.id = id;
		this.date = date;
		this.matrixFirst = matrixFirst;
		this.matrixSecond = matrixSecond;
		this.matrixResult = matrixResult;
	}

	public MatrixHistory(int id, Date date) {
		super();
		this.id = id;
		this.date = date;
		this.matrixFirst = new Matrix(1, 1);
		this.matrixSecond = new Matrix(1, 1);
		this.matrixResult = new Matrix(1, 1);
	}

	public MatrixHistory() {
	}

	@Id
	@Column(name = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "matrix_first")
	public Matrix getMatrixFirst() {
		return matrixFirst;
	}

	public void setMatrixFirst(Matrix matrixFirst) {
		this.matrixFirst = matrixFirst;
	}

	@Column(name = "matrix_second")
	public Matrix getMatrixSecond() {
		return matrixSecond;
	}

	public void setMatrixSecond(Matrix matrixSecond) {
		this.matrixSecond = matrixSecond;
	}

	@Column(name = "matrix_result")
	public Matrix getMatrixResult() {
		return matrixResult;
	}

	public void setMatrixResult(Matrix matrixResult) {
		this.matrixResult = matrixResult;
	}

	@Column(name = "date")
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
